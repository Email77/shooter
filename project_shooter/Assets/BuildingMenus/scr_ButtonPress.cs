using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_ButtonPress : MonoBehaviour
{
    public int menuID;
    public GameObject hoverObject;
    public Sprite selectionSprite;

    public void OnClick()
    {
        GetComponentInParent<scr_MenuMainMenu>().MenuAction(menuID);
    }

    public void OnHoverEnter()
    {
        if (hoverObject != null)
        {
            Image _image = hoverObject.GetComponent<Image>();
            Debug.Log("OnHoverEnter");
            if (hoverObject.GetComponent<Image>().sprite != selectionSprite)
                _image.sprite = selectionSprite;
            Color tempColor = _image.color;
            _image.color = new Color(tempColor.r, tempColor.g, tempColor.b, 1f);
        }
    }

    public void OnHoverExit()
    {
        if (hoverObject != null)
        {
            Debug.Log("OnHoverExit");
            Color tempColor = hoverObject.GetComponent<Image>().color;
            hoverObject.GetComponent<Image>().color = new Color(tempColor.r, tempColor.g, tempColor.b, 0f);
        }
    }
}