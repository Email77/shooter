using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_MenuMainMenu : MonoBehaviour
{
    public GameObject[] openableScripts;
    public int[] inputButtonsToScripts;

    public void MenuAction(int receivedID)
    {
        Debug.Log(receivedID + " was clicked");
        if (openableScripts[inputButtonsToScripts[receivedID]] != null)
        {
            Instantiate(openableScripts[inputButtonsToScripts[receivedID]], GetComponentInParent<Canvas>().transform);
            Destroy(gameObject);
        }
    }
}
