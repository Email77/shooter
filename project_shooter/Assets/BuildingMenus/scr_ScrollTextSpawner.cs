using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_ScrollTextSpawner : MonoBehaviour
{
    public scr_MenuBGTextWordHighlighter textBlockObject;
    public TextBlock[] creatableTexts;

    /// <summary>
    /// you can change to private later, Doesn't really matter, or at least it should be private
    /// </summary>
    public List<scr_MenuBGTextWordHighlighter> created;

    private void Start()
    {
        created = new List<scr_MenuBGTextWordHighlighter>();
        InstantiateNewTextblock();
    }

    public void InstantiateNewTextblock()
    {
        scr_MenuBGTextWordHighlighter placeholder = Instantiate(textBlockObject, gameObject.transform);
        created.Add(placeholder);
        int random = Random.Range(0, creatableTexts.Length);
        placeholder.applyableText = creatableTexts[random].tApplyableText;
        placeholder.keyword = creatableTexts[random].tKeyword;
    }

    public void RemoveFromList(scr_MenuBGTextWordHighlighter received)
    {
        created.Remove(received);
    }

}
[System.Serializable]
public class TextBlock
{
    public string tApplyableText;
    public string tKeyword;
}