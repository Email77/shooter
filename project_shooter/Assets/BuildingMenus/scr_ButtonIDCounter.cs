using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_ButtonIDCounter : MonoBehaviour
{
    public int currentID = 0;

    public void ResetID()
    {
        currentID = 0;
    }
    public void RaiseID()
    {
        currentID++;
    }
}
