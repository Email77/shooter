using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class scr_MenuBGTextWordHighlighter : MonoBehaviour
{
    public string applyableText;
    public string keyword;

    private TextMeshProUGUI _text;
    private RectTransform canvasRect;
    private bool startedOnce;
    private bool responsibleForNext = true;

    IEnumerator Start()
    {
        // It takes three frames after assigning text the rect transform coordinates take a frame to update.
        // Changing position changes the text vertex coordinates values and that takes a frame,
        // so they cant be accessed before the next frame in FindKeyword().
        // Basically it is TextMeshPro delay I think.

        _text = GetComponent<TextMeshProUGUI>();
        AssignOpacity(true);
        ApplyText(applyableText);
        yield return new WaitForEndOfFrame();
        ApplyPosition();
        AssignOpacity(false);
        yield return new WaitForEndOfFrame();
        FindKeyword(keyword);
        startedOnce = true;
    }

    private void Update()
    {
        CheckIfInsideScreenBounds();
        AreWeReadyForNextText();
        MoveUp();
    }

    private void AssignOpacity(bool wantedInvisible)
    {
        if (wantedInvisible)
            _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, 0f);
        else
            _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, 1f);
    }

    private void MoveUp()
    {
        if (canvasRect == null)
            canvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();

        transform.position += new Vector3(0f, canvasRect.rect.height / 20, 0f) * Time.deltaTime;
    }

    private void ApplyPosition()
    {
        if (canvasRect == null)
            canvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();

        GetComponent<RectTransform>().sizeDelta = new Vector2(canvasRect.rect.width / 3, GetComponent<RectTransform>().sizeDelta.y);
        transform.position = new Vector3(canvasRect.rect.width - GetComponent<RectTransform>().sizeDelta.x / 2, 0 - GetComponent<RectTransform>().sizeDelta.y / 2, 0);
    }

    private void CheckIfInsideScreenBounds()
    {
        if (startedOnce)
        {
            if (canvasRect == null)
                canvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
            Vector3 bottomleft = _text.textInfo.characterInfo[_text.textInfo.characterCount - 1].bottomLeft;
            Vector3 worldBottomLeft = _text.transform.TransformPoint(bottomleft);
            if (worldBottomLeft.y > canvasRect.rect.height)
            {
                GetComponentInParent<scr_ScrollTextSpawner>().RemoveFromList(gameObject.GetComponent<scr_MenuBGTextWordHighlighter>());
                Destroy(gameObject);
            }
        }
    }

    private void AreWeReadyForNextText()
    {
        if (startedOnce)
        {
            if (canvasRect == null)
                canvasRect = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
            Vector3 bottomleft = _text.textInfo.characterInfo[_text.textInfo.characterCount - 1].bottomLeft;
            Vector3 worldBottomLeft = _text.transform.TransformPoint(bottomleft);
            if (worldBottomLeft.y > 0)
            {
                if (responsibleForNext)
                {
                    GetComponentInParent<scr_ScrollTextSpawner>().InstantiateNewTextblock();
                    responsibleForNext = false;
                }
            }
        }
    }

    public void ApplyText(string _receivedText)
    {
        _text.text = _receivedText;
    }

    private void FindKeyword(string _keyword)
    {
        for (int i = 0; i < _text.textInfo.wordCount; i++)
        {
            if (_text.textInfo.wordInfo[i].GetWord() == _keyword)
            {
                TMP_WordInfo _info = _text.textInfo.wordInfo[i];
                for (int b = 0; b < _info.characterCount; b++)
                {
                    int charIndex = _info.firstCharacterIndex + b;
                    int meshIndex = _text.textInfo.characterInfo[charIndex].materialReferenceIndex;
                    int vertexIndex = _text.textInfo.characterInfo[charIndex].vertexIndex;

                    Color32 myColor32 = Color.green;
                    Color32[] vertexColors = _text.textInfo.meshInfo[meshIndex].colors32;
                    vertexColors[vertexIndex + 0] = myColor32;
                    vertexColors[vertexIndex + 1] = myColor32;
                    vertexColors[vertexIndex + 2] = myColor32;
                    vertexColors[vertexIndex + 3] = myColor32;
                    _text.textInfo.meshInfo[meshIndex].colors32 = vertexColors;
                }
                _text.UpdateVertexData();
            }
        }
    }
}
